Takes the id of an element and either 'val' or 'text' as parameters. Gets the number that the element contains and formats it appropriately.

<span id="num">1234567</span> would become 12,345.67

Example usage would be:

mdfFormatNum('num', 'text');