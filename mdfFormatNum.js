function mdfFormatNum(el, type) {
    // convert string passed as el to the id of the element
    valId = '#' + el;

    // check whether to use .val() or .text()
    // set num equal to the number the element contains
    if (type == 'val') {
        num = $(valId).val();
    } else if (type == 'text') {
        num = $(valId).text();
    }

    // create new empty array and fill it with the contents of num
    var r = [];
    for (i = 0; i < num.length; i++) {
        r.push(num[i]);
    }

    // get the length of r and put a decimal point two spots from the back
    rlen = r.length;
    spot = rlen - 2;
    r.splice(spot, 0, ".");

    // convert r to a string and get rid of commas
    // then add a comma in each thousands place
    r = r.toString();
    r = r.replace(/,/g, "");
    r = r.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // check whether to use .val() or .text()
    // set element's value to the formatted number
    if (type == 'val' ){
        $(valId).val(r);
    } else if (type == 'text') {
        $(valId).text(r);
    }
}